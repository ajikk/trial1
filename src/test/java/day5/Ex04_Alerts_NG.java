package day5;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

public class Ex04_Alerts_NG {
	WebDriver driver = null;

	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
	}



	@Test
	public void validate_JSAlert() {
		
		
		WebDriverWait  wait = new WebDriverWait(driver, 25);
		
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");
		By byvar = By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button");
		WebElement weJSbutton = driver.findElement(byvar);
		weJSbutton.click();
		

		wait.until(ExpectedConditions.alertIsPresent());

		Alert al = driver.switchTo().alert();
		al.accept();
		
		String expRes = "You successfuly clicked an alert";
		
		String actRes = driver.findElement(By.xpath("//*[@id=\"result\"]")).getText();
		System.out.println();
		if (actRes.equals(expRes)) {
			System.out.println("The " +expRes + " Expected correct message");
		}


	}
	
	
	


	@AfterTest
	public void afterTest() {
		driver.quit();

	}

}
