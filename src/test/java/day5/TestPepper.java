package day5;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestPepper {
  WebDriver driver=null;
  
	@BeforeTest
	public void beforeTest() {
		driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
	}


	/*
	 * 
	 * 	Test #1: Use keyword “Bedsheets”,
		Test #2: Use keyword “Clocks” and
		Test #3: Use keyword “Padlocks”.
		Save these keywords in an Excel (.xls) file, read the search keywords 
		from this file and then execute your test.Write script in TestNG using 
		WebDriver + Google Chrome to test that the results are indeed in Ascending order .
	 * */
	 
	
	@Test
  public void performTest() throws InterruptedException {
		
		driver.get("https://www.pepperfry.com");
		
		//*[@id="search"]
		//*[@id="curSortType"]
		driver.findElement(By.xpath("//*[@id=\"search\"]")).sendKeys("Bedsheets");
		driver.findElement(By.xpath("//*[@id=\"search\"]")).sendKeys(Keys.ENTER);
		

		
		WebDriverWait wait = new WebDriverWait(driver,2);
		  try {
		   if(wait.until(ExpectedConditions.alertIsPresent())==null);
		   System.out.println("Alert present");
		   
		   Actions action = new Actions(driver);
		   WebElement priceSort=driver.findElement(By.xpath("//*[@id=\"curSortType\"]"));
		   action.moveToElement(priceSort).click();
		   System.out.println("Sort clicked");
		   
		  } catch (Exception e) {
		    System.out.println("Alert not present");
		  }
		
	//	WebElement priceSort=driver.findElement(By.xpath("//*[@id=\"curSortType\"]"));

		//

		//action.moveToElement(priceSort).click().moveToElement(driver.findElement(By.xpath("\"//*[@id=\\\"sortBY\\\"]/li[2]/a\"")));
		//action.moveToElement(priceSort).click();
				
  }
}
