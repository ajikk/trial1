package day1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Ex_Google {

	
	public static void main(String args[]) {
		
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Ajith\\eclipse-workspace\\CPSAT_Jul\\drivers\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http://www.google.com");
		System.out.println("PAGE TITLE is : " + driver.getTitle());
		
		By search= By.name("q");
		WebElement searchEl=driver.findElement(search);
		searchEl.sendKeys("CPSAT");
		searchEl.sendKeys(Keys.ENTER);
		System.out.println("PAGE TITLE is : " + driver.getTitle());
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.close();
		
		
		
		
	}
}
