package day2;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ex02BWindowSwitching {

	public static void main(String[] args) {
		
	
		
		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);
		driver.get("https://www.ataevents.org/");
	
		
		String filename = "src\\test\\resources\\screenshots\\imageSocialMedia5.png";
	
		//utils.HelperFunctions.captureScreenShot(driver, filename);
		//By link1 = By.xpath("//a[text()='ATA Virtual Meetup 3']");
		//driver.findElement(link1);
		By byvar = By.xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[*]/div/div/div/div/div/div/h3/a");
		List<WebElement> welst   = driver.findElements(byvar);
		System.out.println(driver.getTitle());
		
		for(WebElement    ele    : welst) {
			ele.click();
		}
		
		
		Set <String> setwindwhandles= driver.getWindowHandles();
		
		for(String wh: setwindwhandles) {
			
		System.out.println(driver.getWindowHandle());
		driver.switchTo().window(wh);
		System.out.println(driver.getTitle());
		}
		driver.quit();
		

	}

}
