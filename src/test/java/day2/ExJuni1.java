package day2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import junit.framework.Assert;

class ExJuni1 {

	
	

	@Test
	void test() {

		WebDriver driver = utils.HelperFunctions.createAppropriateDriver("Chrome",false);

		driver.get("https://www.imdb.com/");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search\"]")).sendKeys("Avengers");
		driver.findElement(By.xpath("//*[@id=\"suggestion-search-button\"]")).click();

		driver.findElement(By.xpath("//*[@id=\"main\"]/div/div[2]/table/tbody/tr[1]/td[2]/a")).click();

		String Dir=driver.findElement(By.xpath("//*[@id='title-overview-widget']/div[2]/div[1]/div[2]/h4")).getText();
		if (Dir.contains("Director") &&
				driver.findElement(By.xpath("//*[@id=\"title-overview-widget\"]/div[2]/div[1]/div[2]/h4//following::a[1]")).getText().contains("Joss Whedon")) 
		System.out.println("The Director is Joss Whedon");
		
		
		else 
			System.out.println("Element not present..");

		String Stars=driver.findElement(By.xpath("//*[@id='title-overview-widget']/div[2]/div[1]/div[4]/h4")).getText();
		
		//Check Chris Evans is present in nodes of Stars
		
		List <WebElement> cast= driver.findElements(By.xpath("//*[@id='title-overview-widget']/div[2]/div[1]/div[4]/h4//following-sibling::a"));
		for(WebElement ele : cast) {
			String str = ele.getText();
			if (str.contains("Chris Evans") && Stars.contains("Stars"))
				break;

		}

		System.out.println("Stars contains Chris Evans");  	
	}

}
